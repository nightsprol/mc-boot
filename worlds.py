import os
import shutil

from config import (
    WORLDS_DIR,
    CONFIGS_DIR,
    DATA_DIR,
    SERVER_PROPS_FNAME,
    SERV_PROPS_IP,
    SERV_PROPS_WORLD,
    SERV_PROPS_MOTD,
    SERV_PROPS_PORT,
)
from network import Dropbox
from properties import Properties


class World:
    METADATA_PROPS = "metadata.properties"
    ARCHIVE_FORMAT = "zip"

    def __init__(self, name):
        self.name = name
        self.dir = os.path.join(WORLDS_DIR, name)
        self.local = True

        # Metadata
        self.version = 0
        self.host = None
        self.port = None

        # Get the world version
        self.read_metadata()

    def initialize(self, localhost, publichost):
        """
        Create the world's directory if it doesn't exist and set the localhost.
        Returns the port specified in the properties (typically 25565).
        """
        if not os.path.isdir(self.dir):
            os.makedirs(self.dir)
            for fname in os.listdir(CONFIGS_DIR):
                shutil.copyfile(
                    os.path.join(CONFIGS_DIR, fname), os.path.join(self.dir, fname)
                )

        server_props = Properties(os.path.join(self.dir, SERVER_PROPS_FNAME))
        server_props.set(SERV_PROPS_IP, localhost)
        server_props.set(SERV_PROPS_WORLD, self.name)
        server_props.set(SERV_PROPS_MOTD, self.name)
        self.port = server_props.get(SERV_PROPS_PORT, 25565)
        server_props.write()

        self.host = publichost

    def read_metadata(self):
        metadata_filepath = os.path.join(self.dir, World.METADATA_PROPS)
        if not os.path.isfile(metadata_filepath):
            return
        metadata = Properties(metadata_filepath)
        self.version = metadata.get("version", 0)

    def write_metadata(self):
        metadata = Properties(os.path.join(self.dir, World.METADATA_PROPS))
        metadata.set("version", self.version)
        metadata.set("host", self.host)
        metadata.set("port", self.port)
        metadata.write()
        return metadata

    def pull_metadata(self):
        return Dropbox.download_file(
            os.path.join(self.dir, World.METADATA_PROPS),
            "/{}/{}".format(self.name, World.METADATA_PROPS),
        )

    def push_metadata(self):
        Dropbox.upload_file(
            os.path.join(self.dir, World.METADATA_PROPS),
            "/{}/{}".format(self.name, World.METADATA_PROPS),
        )

    def pull_world(self):
        zip_name = self.name + "." + World.ARCHIVE_FORMAT

        # Download the archive of the world
        if not Dropbox.download_file(
            os.path.join(DATA_DIR, zip_name), "/{}/{}".format(self.name, zip_name)
        ):
            return False
        # Unpack the archive contents into the world directory
        shutil.unpack_archive(
            os.path.join(DATA_DIR, zip_name), self.dir, World.ARCHIVE_FORMAT
        )
        # Delete the archive from the local file system
        os.remove(os.path.join(DATA_DIR, zip_name))
        return True

    def push_world(self):
        zip_name = self.name + "." + World.ARCHIVE_FORMAT

        # Archive the contents of the world directory
        shutil.make_archive(
            os.path.join(DATA_DIR, self.name), World.ARCHIVE_FORMAT, self.dir
        )
        # Upload the archive of the world
        Dropbox.upload_file(
            os.path.join(DATA_DIR, zip_name), "/{}/{}".format(self.name, zip_name)
        )
        # Delete the archive from the local file system
        os.remove(os.path.join(DATA_DIR, zip_name))

    def startup(self, force=False):
        # Read metadata that exists on disk
        self.read_metadata()

        # No metadata exists in the cloud, this is the first time it's been run.
        # Push the updated metadata to the cloud with our host and port.
        if not self.pull_metadata():
            self.write_metadata()
            self.push_metadata()
            return

        # If metadata exists for the world in the cloud, read it
        cloud_metadata = Properties(os.path.join(self.dir, World.METADATA_PROPS))
        version = cloud_metadata.get("version", None)
        host = cloud_metadata.get("host", None)
        port = cloud_metadata.get("port", None)

        # Check if server is already being hosted by someone else
        if host and port and not force:
            self.local = False
            self.host = host
            self.port = port
        # Otherwise make our intention known that we will host the server
        else:
            if force and host and port:
                print(
                    "WARNING: Overriding a hosted server, this could skew world saves."
                )
            cloud_metadata.set("host", self.host)
            cloud_metadata.set("port", self.port)
            cloud_metadata.write()
            self.push_metadata()

            # Check if we need to download the latest version of the world
            if version > self.version:
                print(
                    "Downloading {} v{}... ".format(self.name, version),
                    end="",
                    flush=True,
                )
                self.pull_world()
                print("done")

                self.version = version

        # Write metadata back out to disk
        self.write_metadata()

    def shutdown(self, backup=True):
        # Only run this if we were running the server
        if not self.local:
            return

        # Update metadata
        if backup:
            self.version += 1
        self.host = None
        self.port = None
        self.write_metadata()

        if backup:
            # Upload the changed world to the cloud
            print(
                "Uploading {} v{}... ".format(self.name, self.version),
                end="",
                flush=True,
            )
            self.push_world()
            print("done")

        # Upload the metadata (do this second to decrease chance of race condition)
        self.push_metadata()

    @classmethod
    def migrate_world(cls, filepath, localhost, publichost):
        if not os.path.isdir(filepath):
            return False, "Invalid filepath " + filepath

        name = os.path.basename(os.path.normpath(filepath))

        if os.path.isdir(os.path.join(WORLDS_DIR, name)):
            return False, "The world {} already exists".format(name)

        # Copy the files to our managed directories
        world = World(name)
        world.initialize(localhost, publichost)

        shutil.copytree(filepath, os.path.join(WORLDS_DIR, name, name))

        # Create files in the cloud
        world.shutdown()

        return True, ""

    @classmethod
    def get_world(cls):
        """
        Interactive helper to create an instance of a world, either from
        an existing world or by creating a new directory structure.
        """
        worlds = Dropbox.listdir("/")

        if not len(worlds):
            print("There are no existing worlds.")
            return World(input("Create a new world: "))

        print("Available worlds:")
        for i in range(len(worlds)):
            print("  {}) {}".format(i, worlds[i]))

        choice = input("Choose a number or enter a new world name: ")

        if not choice.isdigit():
            return World(choice)

        choice = int(choice)
        if choice < 0 or choice > len(worlds):
            print("That world does not exist!")
            exit()

        return World(worlds[choice])
