from contextlib import closing
import json
import os
import socket
import subprocess
import time
import threading
import urllib.request

from common import memoize


class Network:
    """
    The goal in this module is to have platform-independent networking code.

    All the code here has been tested manually, since unfortunately it's kind
    of difficult to write tests for networking code that can have consistent
    results, since they rely both on the network and external resources.

    When possible we'd like to not make multiple network calls during a single
    run of the script, so we memoize the return value of as many method calls
    as we can, assuming that they're unlikely to change during the runtime.
    """

    @memoize
    def get_public_host(self):
        try:
            with urllib.request.urlopen("https://api.ipify.org") as response:
                return response.read().decode("utf-8")
        except Exception as e:
            print(e)
            exit()

    @memoize
    def get_local_host(self):
        try:
            with closing(socket.socket(socket.AF_INET, socket.SOCK_DGRAM)) as sock:
                sock.connect(("8.8.8.8", 80))
                return sock.getsockname()[0]
        except Exception as e:
            print(e)
            exit()

    # TODO: make this cross platform
    @memoize
    def get_default_gateway(self):
        ipconfig = subprocess.Popen(
            "ipconfig", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        )

        found = False

        for line in ipconfig.stdout.readlines():
            if found:
                return line.decode("utf-8").strip()
            if b"Default Gateway" in line:
                found = True

        return None

    def check_port_forward(self, port, attempts=1):
        """
        Returns True if the specified port is being forwarded on the localhost
        """

        # Used to terminate the thread
        class ThreadData:
            def __init__(self):
                self.kill = False

        data = ThreadData()
        thread = threading.Thread(target=self._echo_server, args=(port, data))
        thread.start()
        result = -1

        for i in range(attempts):
            result = self._check_socket(self.get_public_host(), port, timeout=1)
            if attempts > 1 and result != 0:
                print(f"Failed to ping server... ({i}/{attempts})")
                time.sleep(1)
            else:
                data.kill = True
                break
        thread.join()

        return result == 0

    #
    # Private helper methods
    #

    def _check_socket(self, host, port, timeout=None):
        """
        Returns True if the host is reachable on the port
        """
        try:
            with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
                sock.settimeout(timeout)
                return sock.connect_ex((host, port))
        except Exception as e:
            print(e)
            exit()

    def _echo_server(self, port, data):
        """
        Opens up a socket on localhost:port and listens until data.kill = True
        """
        try:
            with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                sock.bind((self.get_local_host(), port))
                sock.listen(10)

                while not data.kill:
                    pass
        except Exception as e:
            print(e)
            exit()


class Dropbox:
    # TODO: require a password or secret for separate worlds
    APP_AUTH = "xImJ-nvi2TUAAAAAAAAFgcYGJlv0nRiy7uw_pMMhYRQIElNcyk_DYz4hd2FgSZ7N"

    @classmethod
    def get_filename(self, filepath):
        return os.path.basename(os.path.normpath(filepath))

    @classmethod
    def get_url(cls, endpoint):
        return "https://content.dropboxapi.com/2" + os.path.join("/", endpoint)

    @classmethod
    def upload_file(cls, local_path, server_path):
        with open(local_path, "rb") as f:
            request = urllib.request.Request(
                cls.get_url("/files/upload"),
                headers=cls._fix_dict(
                    {
                        "Authorization": "Bearer " + cls.APP_AUTH,
                        "Content-Type": "application/octet-stream",
                        "Dropbox-API-Arg": {
                            "path": os.path.join("/", server_path),
                            "mode": "overwrite",
                            "mute": False,
                        },
                    }
                ),
                data=f.read(),
            )

        urllib.request.urlopen(request)

    @classmethod
    def download_file(cls, local_path, server_path):
        """
        Download the requested file from the Dropbox server.
        Returns False if the file does not exist, or True on success.
        """
        request = urllib.request.Request(
            cls.get_url("/files/download"),
            headers=cls._fix_dict(
                {
                    "Authorization": "Bearer " + cls.APP_AUTH,
                    "Dropbox-API-Arg": {"path": os.path.join("/", server_path)},
                }
            ),
        )

        try:
            with urllib.request.urlopen(request) as resp, open(local_path, "wb") as f:
                f.write(resp.read())
        except urllib.error.HTTPError as e:
            if e.code == 404 or e.code == 409:
                return False
            else:
                raise e
        return True

    @classmethod
    def listdir(cls, server_path):
        """
        Returns a list of the contents in the specified directory.
        """
        if server_path == "/":
            server_path = ""
        else:
            server_path = os.path.join("/", server_path)

        request = urllib.request.Request(
            "https://api.dropboxapi.com/2/files/list_folder",
            headers=cls._fix_dict(
                {
                    "Authorization": "Bearer " + cls.APP_AUTH,
                    "Content-Type": "application/json",
                }
            ),
            data=json.dumps({"path": server_path}).encode(),
        )

        try:
            with urllib.request.urlopen(request) as resp:
                d = json.loads(resp.read())
                return [x["name"] for x in d["entries"]]
        except urllib.error.HTTPError as e:
            if e.code == 404 or e.code == 409:
                return []
            else:
                raise e

    @classmethod
    def _fix_dict(self, d):
        for key, value in d.items():
            if type(value) is dict:
                d[key] = json.dumps(value)
        return d
