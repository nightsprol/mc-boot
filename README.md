# mc-boot

A Minecraft server manager all-in-one.

## Features

Easy server setup:
- downloads all the required files
- performs the initial server setup
- sets server properties automatically (local IPv4, world name)
- helps set up port forwarding to host a public server
- runs the server and cleans it up after it finishes

Distributed worlds:
- allows easy management of multiple worlds
- syncs the different worlds to Dropbox
- allows any user of the script to host an existing world if no one is already
- displays the host and port to connect to if a server is up currently

## Setup

You can get a copy of the source code with the following:

```sh
$ git clone https://gitlab.com/nightsprol/mc-boot.git
$ cd mc-boot/
```

You will need Python 3 and the latest version of Java installed.

```sh
$ python --version
$ java --version
```

If both of those are recognized by your shell, you should be good to go!

## Usage

Running mc-boot for the first time is easy, and it will guide you through the one-time setup:

```sh
$ python mcboot.py --help
```

There are a couple command line options that you can provide for convenience, but for the most part it will handle everything on its own without a problem.

```sh
# Migrate an existing world to be managed by mc-boot
$ python mcboot.py --migrate path/to/world/directory/

# Open the Minecraft launcher (downloads it if necessary), only works on Windows
$ python mcboot.py --launch

# Start the server with a specific about of GB allocated in RAM (4 GB in this case)
$ python mcboot.py --mem-util 4
```

You can combine any number of command line options in a single run of the script.

## Contributing

todo

### Testing

todo