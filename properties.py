import os


class Property:
    def __init__(self, key, value="", comment=""):
        self.key = key
        self.value = value.strip() if type(value) is str else value
        self.comment = comment.strip()

    def getValue(self):
        value = self.value.lower()
        if len(value) == 0:
            return None
        elif value == "true":
            return True
        elif value == "false":
            return False
        elif value.isdigit():
            return int(value)
        elif value.count(".") == 1 and value.replace(".", "").isdigit():
            return float(value)
        else:
            return str(value)

    def __repr__(self):
        s = ""
        if self.key:
            if self.value is None:
                s = self.key + "="
            else:
                s = "{}={}".format(self.key, str(self.value).strip())
        if self.comment:
            if s:
                s += " "
            s += self.comment
        return s.strip()

    @classmethod
    def parse(cls, line):
        comment = line[line.find("#") :]
        if "#" in line:
            line = line[: line.find("#") - 1]

        if "=" not in line:
            if comment:
                return Property(None, "", comment)
            return None

        key = line[: line.find("=")]
        value = line[line.find("=") + 1 :]
        return Property(key, value, comment)


# Class for reading/writing *.properties files
class Properties:
    def __init__(self, filepath):
        self._filepath = filepath
        self._properties = {}
        self._lines = []

        if os.path.isfile(filepath):
            with open(filepath, "r") as props:
                for line in props:
                    prop = Property.parse(line)
                    if prop.key:
                        self._properties[prop.key] = prop
                    self._lines.append(prop)

    def __contains__(self, key):
        return key in self._properties

    def get(self, key, default=None):
        prop = self._properties.get(key, None)
        return prop.getValue() if prop is not None else default

    def set(self, key, value):
        prop = self._properties.get(key)
        if prop:
            prop.value = value
        else:
            prop = Property(key, value)
            self._properties[key] = prop
            self._lines.append(prop)

    def write(self, filepath=None):
        with open(self._filepath if not filepath else filepath, "w") as props:
            for prop in self._lines:
                props.write(str(prop) + "\n")
