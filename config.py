from os.path import dirname, join, realpath

# Directories
DATA_DIR = join(dirname(realpath(__file__)), "data")
LAUNCHER_DIR = join(DATA_DIR, "launcher")
WORLDS_DIR = join(DATA_DIR, "worlds")
CONFIGS_DIR = join(DATA_DIR, "configs")

# Filenames
SERVER_FNAME = "server.jar"
LAUNCHER_FNAME = "launcher.exe"
SERVER_PROPS_FNAME = "server.properties"
EULA_FNAME = "eula.txt"
SERVER_ICON_FNAME = "server-icon.png"

# Server properties
SERV_PROPS_IP = "server-ip"
SERV_PROPS_PORT = "server-port"
SERV_PROPS_WORLD = "level-name"
SERV_PROPS_MOTD = "motd"

CONFIGS_FILE_MIN = 5
WORLD_NAME = "world"
SERVER_CMD = "java {0} -jar " + join(DATA_DIR, SERVER_FNAME) + " nogui"

# TODO: scrape "https://minecraft.net/en-us/download/server/" for server.jar url
SERVER_URL = "https://launcher.mojang.com/v1/objects/bb2b6b1aefcd70dfd1892149ac3a215f6c636b07/server.jar"
LAUNCHER_URL = "https://launcher.mojang.com/download/Minecraft.exe"
