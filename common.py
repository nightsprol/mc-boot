import os
import shutil
import signal
import subprocess
import time

from config import CONFIGS_FILE_MIN, SERVER_CMD


def ask_user(prompt):
    """ Simple wrapper around input() for y/n questions """
    _input = input(prompt + " (y/n): ").lower()
    return _input == "yes" or _input == "y"


def clean_dir(directory, whitelist=[], dirs=False):
    """ Remove all files in a directory that are not in the whitelist """
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)
        if filename not in whitelist:
            if not os.path.isdir(filepath):
                os.remove(filepath)
            elif dirs:
                shutil.rmtree(filepath)


def server_boot(directory, options="", hide_output=False):
    """ Wrapper around subprocess.run for starting the server """
    args = SERVER_CMD.format(options).split()

    try:
        old_dir = os.getcwd()
        os.chdir(directory)
        if hide_output:
            with open(os.devnull, "w") as f:
                subprocess.run(args, stdout=f, stderr=f)
        else:
            subprocess.run(args)
    except KeyboardInterrupt:
        print("\nServer was terminated with Ctrl-c! Please use /stop instead.")
    except Exception as e:
        raise Exception(e)
    finally:
        os.chdir(old_dir)


def server_configs(directory, options=""):
    """
    Run the server just long enough to generate the configs.

    You MUST run server_boot() first and agree to the EULA before calling this,
    or else it will hang indefinitely.
    """
    args = SERVER_CMD.format(options).split()

    old_dir = os.getcwd()
    os.chdir(directory)
    with open(os.devnull, "w") as f:
        proc = subprocess.Popen(args, stdout=f, stderr=f)

    while len(os.listdir(directory)) <= CONFIGS_FILE_MIN:
        time.sleep(1)

    os.kill(proc.pid, signal.SIGTERM)
    time.sleep(1)
    os.chdir(old_dir)


def memoize(method):
    """ Class method decorator to memoize return values """

    def inner(self, *args):
        prop = getattr(self, "_" + method.__name__, None)
        if prop is not None:
            return prop
        prop = method(args)
        setattr(self, "_" + method.__name__, prop)
        return prop

    return inner
