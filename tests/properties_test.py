import unittest
import os

import properties

# Run with `python -m unittest tests.properties_test` in mc-boot/
class TestProperties(unittest.TestCase):
    def setUp(self):
        filepath = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "test.properties"
        )
        self.properties = properties.Properties(filepath)

    def _get(self, key, default=None):
        return self.properties.get(key, default)

    def _get_prop(self, key, default=None):
        return self.properties._properties.get(key, default)

    def test_boolean(self):
        self.assertEqual(self._get("boolean"), True)

    def test_int(self):
        self.assertEqual(self._get("int"), 42)

    def test_float(self):
        self.assertEqual(self._get("float"), 3.14)

    def test_string(self):
        self.assertEqual(self._get("string"), "test")

    def test_string_whitespace(self):
        self.assertEqual(self._get("string_whitespace"), "string with whitespace")

    def test_null(self):
        self.assertEqual(self._get("null"), None)

    def test_null_comment(self):
        prop = self._get_prop("null_comment")
        self.assertEqual(prop.getValue(), None)
        self.assertEqual(prop.comment, "# comment")

    def test_comment(self):
        prop = self._get_prop("comment")
        self.assertEqual(prop.getValue(), "comment with whitespace")
        self.assertEqual(prop.comment, "# inline comment")


if __name__ == "__main__":
    unittest.main()
