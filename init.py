import os
import shutil
import urllib.request

from common import ask_user, server_boot, server_configs
from config import (
    WORLDS_DIR,
    CONFIGS_DIR,
    DATA_DIR,
    SERVER_FNAME,
    LAUNCHER_DIR,
    LAUNCHER_FNAME,
    SERVER_URL,
    LAUNCHER_URL,
    EULA_FNAME,
    CONFIGS_FILE_MIN,
)
from properties import Properties


class Init:
    """
    This class is meant for one-time initialization, but it's designed so
    that it can be called at start up every time to see if anything needs
    to be taken care of.

    You should primarily use Init.run() which will take care of everything.

    All other functions are requires_<name>() and handle_<name>(). Each of
    the requires functions should return True if the corresponding handle
    function needs to be run. The handle functions should then return True
    if it completed successfully, or False on failure.
    """

    # Configurable options
    download_launcher = False

    @classmethod
    def run(cls):
        if not cls.requires_init():
            return True

        print("Running one time initialization steps...")

        # Safe to call without checking, has no side effects
        cls.handle_directories()

        if cls.requires_downloads() and not cls.handle_downloads():
            return False

        if cls.requires_server() and not cls.handle_server():
            return False

        if cls.requires_eula() and not cls.handle_eula():
            return False

        if cls.requires_configs() and not cls.handle_configs():
            return False

        print("Initialization is complete!\n")
        return True

    @classmethod
    def requires_init(cls):
        return (
            cls.requires_directories()
            or cls.requires_downloads()
            or cls.requires_server()
            or cls.requires_eula()
            or cls.requires_configs()
        )

    @classmethod
    def requires_directories(cls):
        return (
            not os.path.isdir(DATA_DIR)
            or not os.path.isdir(CONFIGS_DIR)
            or not os.path.isdir(WORLDS_DIR)
            or (cls.download_launcher and not os.path.isdir(os.path.join(LAUNCHER_DIR)))
        )

    @classmethod
    def handle_directories(cls):
        os.makedirs(CONFIGS_DIR, exist_ok=True)
        os.makedirs(WORLDS_DIR, exist_ok=True)
        if cls.download_launcher:
            os.makedirs(LAUNCHER_DIR, exist_ok=True)
        return True

    @classmethod
    def requires_downloads(cls):
        return not os.path.isfile(os.path.join(DATA_DIR, SERVER_FNAME)) or (
            cls.download_launcher
            and not os.path.isfile(os.path.join(LAUNCHER_DIR, LAUNCHER_FNAME))
        )

    @classmethod
    def handle_downloads(cls):
        downloads = [(DATA_DIR, SERVER_FNAME, SERVER_URL)]

        if cls.download_launcher:
            downloads.append((LAUNCHER_DIR, LAUNCHER_FNAME, LAUNCHER_URL))

        # Download required files
        for directory, filename, url in downloads:
            filepath = os.path.join(directory, filename)
            if os.path.isfile(filepath):
                continue

            print("Downloading", filename + "... ", end="", flush=True)
            try:
                with urllib.request.urlopen(url) as response, open(
                    filepath, "wb"
                ) as out_file:
                    shutil.copyfileobj(response, out_file)
                print("done")
            except Exception as e:
                print("failed!\nException:", e)
                return False

        return True

    @classmethod
    def requires_server(cls):
        return not os.path.isfile(os.path.join(CONFIGS_DIR, EULA_FNAME))

    @classmethod
    def handle_server(cls):
        try:
            print(
                "Running {} to generate EULA... ".format(SERVER_FNAME),
                end="",
                flush=True,
            )
            server_boot(CONFIGS_DIR, hide_output=True)
            print("done")
        except Exception as e:
            print("failed!\nException: {}".format(e))
            return False

        for fname in os.listdir(CONFIGS_DIR):
            if os.path.isdir(os.path.join(CONFIGS_DIR, fname)):
                shutil.rmtree(os.path.join(CONFIGS_DIR, fname))

        return True

    @classmethod
    def requires_eula(cls):
        if not os.path.isfile(os.path.join(CONFIGS_DIR, EULA_FNAME)):
            return False
        eula = Properties(os.path.join(CONFIGS_DIR, EULA_FNAME))
        return not eula.get("eula", False)

    @classmethod
    def handle_eula(cls):
        eula = Properties(os.path.join(CONFIGS_DIR, EULA_FNAME))

        print("Mojang's End User License Agreement (EULA):")
        with open(os.path.join(CONFIGS_DIR, EULA_FNAME), "r") as fin:
            for line in fin:
                print("  " + line.strip())
        if not ask_user("Do you agree to the EULA?"):
            print("You must agree to the EULA to complete the setup.")
            print(
                "You can manually change it in", os.path.join(CONFIGS_DIR, EULA_FNAME)
            )
            return False

        # Modify the EULA
        eula.set("eula", True)
        eula.write()
        print("You have agreed to Mojang's EULA.")

        return True

    @classmethod
    def requires_configs(cls):
        return len(os.listdir(CONFIGS_DIR)) <= CONFIGS_FILE_MIN

    @classmethod
    def handle_configs(cls):
        try:
            print(
                "Running {} to generate config files... ".format(SERVER_FNAME),
                end="",
                flush=True,
            )
            server_configs(CONFIGS_DIR)
            print("done")
        except Exception as e:
            print("failed!\nException: {}".format(e))
            return False

        for fname in os.listdir(CONFIGS_DIR):
            if os.path.isdir(os.path.join(CONFIGS_DIR, fname)):
                shutil.rmtree(os.path.join(CONFIGS_DIR, fname))

        return True
