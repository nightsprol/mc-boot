from argparse import ArgumentParser
import os
import subprocess
import webbrowser

from common import ask_user, server_boot
from config import (
    LAUNCHER_DIR,
    LAUNCHER_FNAME,
)
from init import Init
from network import Network
from worlds import World

parser = ArgumentParser()
parser.add_argument(
    "-m",
    "--memory",
    type=int,
    default=4,
    help="Amount of memory to use for the server (in GB). Defaults to 4 GB.",
)
parser.add_argument(
    "-l",
    "--launch",
    action="store_true",
    help="Open the Minecraft launcher before starting the server.",
)
parser.add_argument(
    "--migrate",
    type=str,
    default=None,
    help="Migrate an existing world to mc-boot by providing the filepath to it.",
)
parser.add_argument(
    "--no-backup",
    action="store_true",
    help="Don't backup the world after running the server. For debugging only.",
)
parser.add_argument(
    "-f",
    "--force",
    action="store_true",
    help="Force hosting of the world, even if it says it is being hosted.",
)
args = parser.parse_args()

# Configure init before running
Init.download_launcher = args.launch

# Perform one time initialization (unless it goes wrong, then it can be many times)
if not Init.run():
    exit()

# Create an instance of the Network class which prevents unnecessary network requests
network = Network()

# Get the user's local IPv4 address and public IP
local_host = network.get_local_host()
public_host = network.get_public_host()

# Migrate an existing world so that it can be managed by mc-boot
if args.migrate:
    success, msg = World.migrate_world(args.migrate, local_host, public_host)
    if not success:
        print("Failed to migrate:", msg)

# Get the world that the user would like to host
world = World.get_world()
world.initialize(local_host, public_host)

# This will handle distributed worlds in the cloud
world.startup(force=args.force)

# Inform user that they must change port forwarding settings
if world.local and not network.check_port_forward(world.port):
    print("\nYou are not currently port forwarding.")
    print("If you don't port forward, you will only be able to play over LAN.")
    print("Forward port {} to IP {}".format(world.port, local_host))
    if ask_user("Open router settings for port forwarding?"):
        gateway = network.get_default_gateway()
        print("Opening your router settings at {}...".format(gateway))
        webbrowser.open_new_tab("http://" + gateway)

# Start the Minecraft launcher
if args.launch:
    print("\nOpening the Minecraft launcher...")
    subprocess.Popen(os.path.join(LAUNCHER_DIR, LAUNCHER_FNAME))

# Start up the server
if world.local or args.force:
    try:
        print(
            "\nStarting {} v{} with {} GB of RAM...".format(
                world.name, world.version, args.memory
            )
        )
        print("  Public IP: {}:{}".format(public_host, world.port))
        print("  Local IP:  {}:{}\n".format(local_host, world.port))
        server_boot(world.dir, options="-Xmx{0}G -Xms{0}G".format(args.memory))
        print("\nServer was shutdown gracefully.")
    except Exception as e:
        print("\nException:", str(e))
    finally:
        world.shutdown(not args.no_backup)
else:
    print(world.name, "is already being hosted.")
    print("  Connect to {}:{}".format(world.host, world.port))
